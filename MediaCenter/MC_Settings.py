from enigma import eTimer
from Screens.Screen import Screen
from Screens.Standby import TryQuitMainloop
from Screens.ServiceInfo import ServiceInfoList, ServiceInfoListEntry
from Components.ActionMap import ActionMap, NumberActionMap
from Components.Pixmap import Pixmap, MovingPixmap
from Components.Label import Label
from Screens.MessageBox import MessageBox
from Components.Sources.List import List
from Components.Sources.StaticText import StaticText
from Components.MenuList import MenuList
from Components.ConfigList import ConfigList, ConfigListScreen
from Components.Console import Console
from Components.ScrollLabel import ScrollLabel
from Components.config import *

from Components.Button import Button

from Tools.Directories import resolveFilename, fileExists, pathExists, createDir, SCOPE_MEDIA
from Components.FileList import FileList
from Components.AVSwitch import AVSwitch
from Plugins.Plugin import PluginDescriptor

try:
	from twisted.web.client import getPage
except Exception, e:
	print "Media Center: Import twisted.web.client fehlgeschlagen"

from os import path, walk

from enigma import eServiceReference
import os

#------------------------------------------------------------------------------------------
def getAspect():
	val = AVSwitch().getAspectRatioSetting()
	return val/2
	
class MC_Settings(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)

		list = []
		#list.append(("Titel", "nothing", "entryID", "weight"))
		list.append(("Globale Einstellungen", "MCS_GlobalSettings", "menu_globalsettings", "50"))
		list.append(("Skin Auswahl", "MCS_SkinSelector", "menu_skinselector", "50"))
		list.append(("Ende", "MCS_Exit", "menu_exit", "50"))
		self["menu"] = List(list)
		self["title"] = StaticText("")
		
		self["actions"] = ActionMap(["OkCancelActions"],
		{
			"cancel": self.Exit,
			"ok": self.okbuttonClick
		}, -1)

	def okbuttonClick(self):
		print "okbuttonClick"
		selection = self["menu"].getCurrent()
		if selection is not None:
			if selection[1] == "MCS_GlobalSettings":
				self.session.open(MCS_GlobalSettings)
			elif selection[1] == "MCS_SkinSelector":
				self.session.open(MCS_SkinSelector)
			elif selection[1] == "MCS_Exit":
				self.close(0)
			else:
				self.session.open(MessageBox,("Error: Could not find plugin %s\ncoming soon ... :)") % (selection[1]),  MessageBox.TYPE_INFO)
			
	def Ok(self):
		self.session.open(MPD_PictureViewer)
		
	def Exit(self):
		self.close(0)
		


#------------------------------------------------------------------------------------------

class MCS_GlobalSettings(Screen):
	skin = """
		<screen position="160,220" size="400,120" title="Media Center - Global Settings" >
			<widget name="configlist" position="10,10" size="380,100" />
		</screen>"""
	
	def __init__(self, session):
		self.skin = MCS_GlobalSettings.skin
		Screen.__init__(self, session)

		self["actions"] = NumberActionMap(["SetupActions","OkCancelActions"],
		{
			"ok": self.keyOK,
			"cancel": self.close,
			"left": self.keyLeft,
			"right": self.keyRight,
			"0": self.keyNumber,
			"1": self.keyNumber,
			"2": self.keyNumber,
			"3": self.keyNumber,
			"4": self.keyNumber,
			"5": self.keyNumber,
			"6": self.keyNumber,
			"7": self.keyNumber,
			"8": self.keyNumber,
			"9": self.keyNumber
		}, -1)
		
		self.list = []
		self["configlist"] = ConfigList(self.list)
		self.list.append(getConfigListEntry(_("Sprache"), config.plugins.mc_globalsettings.language))
		self.list.append(getConfigListEntry(_("Zeige im Hauptmenue"), config.plugins.mc_globalsettings.showinmainmenu))
		self.list.append(getConfigListEntry(_("Zeige in Erweiterungen"), config.plugins.mc_globalsettings.showinextmenu))
#		self.list.append(getConfigListEntry(_("Check for Updates on Startup"), config.plugins.mc_globalsettings.checkforupdate))
		
	def keyLeft(self):
		self["configlist"].handleKey(KEY_LEFT)

	def keyRight(self):
		self["configlist"].handleKey(KEY_RIGHT)
		
	def keyNumber(self, number):
		self["configlist"].handleKey(KEY_0 + number)

	def keyOK(self):
		config.plugins.mc_globalsettings.save()
		self.close()
		
			
#------------------------------------------------------------------------------------------
# (c) 2006 Stephan Reichholf
# B0rked by Homey :)

class MCS_SkinSelector(Screen):
	skin = """
		<screen position="75,138" size="600,320" title="Choose your Skin" >
			<widget name="SkinList" position="10,10" size="275,300" scrollbarMode="showOnDemand" />
			<widget name="Preview" position="305,45" size="280,210" alphatest="on"/>
		</screen>
		"""

	skinlist = []
	root = "/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/skins/"

	def __init__(self, session, args = None):

		self.skin = MCS_SkinSelector.skin
		Screen.__init__(self, session)

		self.skinlist = []
		self.previewPath = ""

		path.walk(self.root, self.find, "")

		self.skinlist.sort()
		self["SkinList"] = MenuList(self.skinlist)
		self["Preview"] = Pixmap()

		self["actions"] = NumberActionMap(["WizardActions", "InputActions", "EPGSelectActions"],
		{
			"ok": self.ok,
			"back": self.close,
			"up": self.up,
			"down": self.down,
			"left": self.left,
			"right": self.right
		}, -1)
		
		self.onLayoutFinish.append(self.layoutFinished)

	def layoutFinished(self):
		tmp = config.plugins.mc_globalsettings.currentskin.path.value.find('/skin.xml')
		if tmp != -1:
			tmp = config.plugins.mc_globalsettings.currentskin.path.value[:tmp]
			idx = 0
			for skin in self.skinlist:
				if skin == tmp:
					break
				idx += 1
			if idx < len(self.skinlist):
				self["SkinList"].moveToIndex(idx)
		self.loadPreview()

	def up(self):
		self["SkinList"].up()
		self.loadPreview()

	def down(self):
		self["SkinList"].down()
		self.loadPreview()

	def left(self):
		self["SkinList"].pageUp()
		self.loadPreview()

	def right(self):
		self["SkinList"].pageDown()
		self.loadPreview()

	def find(self, arg, dirname, names):
		for x in names:
			if x == "skin.xml":
				if dirname <> self.root:
					foldername = dirname.split('/')
					subdir = foldername[-1]
					self.skinlist.append(subdir)
				else:
					subdir = "Default Skin"
					self.skinlist.append(subdir)

	def ok(self):
		if self["SkinList"].getCurrent() == "Default Skin":
			skinfile = "BlackWhiteHD/skin.xml"
		else:
			skinfile = self["SkinList"].getCurrent()+"/skin.xml"

		print "Skinselector: Selected Skin: "+self.root+skinfile
		config.plugins.mc_globalsettings.currentskin.path.value = skinfile
		config.plugins.mc_globalsettings.currentskin.path.save()
		restartbox = self.session.openWithCallback(self.restartGUI,MessageBox,_("GUI benoetigt neustart\nJetzt GUI neu starten?"), MessageBox.TYPE_YESNO)
		restartbox.setTitle(_("GUI jetzt neu starten?"))

	def loadPreview(self):
		if self["SkinList"].getCurrent() == "Default Skin":
			pngpath = self.root+"/preview.png"
		else:
			pngpath = self.root+self["SkinList"].getCurrent()+"/preview.png"

		if not path.exists(pngpath):
			# FIXME: don't use hardcoded path
			pngpath = "/usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/skins/noprev.png"

		if self.previewPath != pngpath:
			self.previewPath = pngpath

		self["Preview"].instance.setPixmapFromFile(self.previewPath)

	def restartGUI(self, answer):
		if answer is True:
			self.session.open(TryQuitMainloop, 3)

	
#------------------------------------------------------------------------------------------