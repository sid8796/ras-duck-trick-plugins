import os
from enigma import eTimer
from Components.ActionMap import ActionMap
from Components.Sources.List import List
from Components.Sources.StaticText import StaticText
from Components.ConfigList import ConfigList
from Components.config import *
from Screens.Screen import Screen
from Screens.MessageBox import MessageBox
from Plugins.Plugin import PluginDescriptor

try:
	from twisted.web.client import getPage
except Exception, e:
	print "Media Center: twisted.web.client konnte nicht geladen werden"

# MC Plugins
from MC_AudioPlayer import MC_AudioPlayer
from MC_VideoPlayer import MC_VideoPlayer
from MC_RadioPlayer import *
from MC_PictureViewer import MC_PictureViewer
from MC_Settings import MC_Settings
from MC_Unwetter import *

#------------------------------------------------------------------------------------------
	
class DMC_MainMenu(Screen):
	def __init__(self, session):
		Screen.__init__(self, session)

		# Disable OSD Transparency
		try:
			self.can_osd_alpha = open("/proc/stb/video/alpha", "r") and True or False
		except:
			self.can_osd_alpha = False
		if self.can_osd_alpha:
			open("/proc/stb/video/alpha", "w").write(str("255"))

		# Show Background MVI
		#os.system("/usr/bin/showiframe /usr/lib/enigma2/python/Plugins/Extensions/MediaCenter/icons/background.mvi &")
		
		list = []
		#list.append(("Titel", "nothing", "entryID", "weight"))
		list.append(("Meine Musik", "MC_AudioPlayer", "menu_music", "50"))
		list.append(("Meine Videos", "MC_VideoPlayer", "menu_video", "50"))
		list.append(("Meine Bilder", "MC_PictureViewer", "menu_pictures", "50"))
		list.append(("Webradio Player", "MC_RadioPlayer", "menu_radio", "50"))
		list.append(("Unwetter Info", "MC_unWeatherInfo", "menu_unweather", "50"))
		list.append(("Einstellungen", "MC_Settings", "menu_settings", "50"))
		#list.append(("Exit", "Exit", "menu_exit", "50"))
		self["menu"] = List(list)
		self["title"] = StaticText("")
		self["welcomemessage"] = StaticText("")
		
		self["actions"] = ActionMap(["OkCancelActions"],
		{
			"cancel": self.Exit,
			"ok": self.okbuttonClick
		}, -1)

	def okbuttonClick(self):
		print "okbuttonClick"
		selection = self["menu"].getCurrent()
		if selection is not None:
			if selection[1] == "MC_VideoPlayer":
				self.session.open(MC_VideoPlayer)
			elif selection[1] == "MC_PictureViewer":
				self.session.open(MC_PictureViewer)
			elif selection[1] == "MC_AudioPlayer":
				self.session.open(MC_AudioPlayer)
			elif selection[1] == "MC_RadioPlayer":
				self.session.open(SHOUTcastMCWidget)
			elif selection[1] == "MC_unWeatherInfo":
				self.session.open(UnwetterMain)
			elif selection[1] == "MC_Settings":
				self.session.open(MC_Settings)
			elif selection[1] == "Exit":
				self.Exit()
			else:
				self.session.open(MessageBox,("Fehler: Kann Plugin nicht finden: %s\nOb das noch kommt??? :)") % (selection[1]),  MessageBox.TYPE_INFO)

	def error(self, error):
		self.session.open(MessageBox,("UNERWARTETER FEHLER:\n%s") % (error),  MessageBox.TYPE_INFO)

	def Ok(self):
		self.session.open(MPD_PictureViewer)
		
	def Exit(self):
		# Restart old service
		#self.session.nav.stopService()
		#self.session.nav.playService(self.oldService)
		
		## Restore OSD Transparency Settings
		#os.system("echo " + hex(0)[2:] + " > /proc/stb/vmpeg/0/dst_top")
		#os.system("echo " + hex(0)[2:] + " > /proc/stb/vmpeg/0/dst_left")
		#os.system("echo " + hex(720)[2:] + " > /proc/stb/vmpeg/0/dst_width")
		#os.system("echo " + hex(576)[2:] + " > /proc/stb/vmpeg/0/dst_height")

		if self.can_osd_alpha:
			try:
				open("/proc/stb/video/alpha", "w").write(str(config.av.osd_alpha.value))
			except:
				print "OSD Transparenz konnte nicht gesetzt werden."
		#kill osd Pic
		#os.system("/usr/bin/killall -9 showiframe &")
		
		#configfile.save()
		self.close()
		
#------------------------------------------------------------------------------------------

def main(session, **kwargs):
	session.open(DMC_MainMenu)

def menu(menuid, **kwargs):
	if menuid == "mainmenu":
		return [(_("Media Center (deutsch)"), main, "dmc_mainmenu", 44)]
	return []

def Plugins(**kwargs):
	if config.plugins.mc_globalsettings.showinmainmenu.value == True and config.plugins.mc_globalsettings.showinextmenu.value == True:
		return [
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", icon="plugin.png", where = PluginDescriptor.WHERE_PLUGINMENU, fnc = main),
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", where = PluginDescriptor.WHERE_MENU, fnc = menu),
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", icon="plugin.png", where = PluginDescriptor.WHERE_EXTENSIONSMENU, fnc=main)]	
	elif config.plugins.mc_globalsettings.showinmainmenu.value == True and config.plugins.mc_globalsettings.showinextmenu.value == False:
		return [
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", icon="plugin.png", where = PluginDescriptor.WHERE_PLUGINMENU, fnc = main),
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", where = PluginDescriptor.WHERE_MENU, fnc = menu)]
	elif config.plugins.mc_globalsettings.showinmainmenu.value == False and config.plugins.mc_globalsettings.showinextmenu.value == True:
		return [
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", icon="plugin.png", where = PluginDescriptor.WHERE_PLUGINMENU, fnc = main),
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", icon="plugin.png", where = PluginDescriptor.WHERE_EXTENSIONSMENU, fnc=main)]
	else:
		return [
			PluginDescriptor(name = "Media Center", description = "Media Center Plugin (VIP-DE Edition)", icon="plugin.png", where = PluginDescriptor.WHERE_PLUGINMENU, fnc = main)]
