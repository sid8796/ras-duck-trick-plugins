import os
import urllib
import libxml2
import time

from Screens.Screen import Screen
from Components.Label import Label
from Components.Pixmap import Pixmap
from Components.AVSwitch import AVSwitch
from Components.ActionMap import ActionMap
from Plugins.Plugin import PluginDescriptor
from enigma import ePicLoad
from enigma import eTimer
from Screens.MessageBox import MessageBox

xmlfile = libxml2.parseFile( '/usr/lib/enigma2/python/Plugins/Extensions/MyViewer/cam.xml' )

# parse Bild url
bild = xmlfile.xpathEval('*/cam/url')
url = bild[0]

# parse control url
control = xmlfile.xpathEval('*/cam/control')
control = control[0]

url=(url.prop('Bild'))
control = (control.prop('Control'))

# definiert das image file
filename = "/usr/lib/enigma2/python/Plugins/Extensions/MyViewer/picture/image.jpg"
URL = '%s' % url
CONTROL = '%s' % control

class ViewFull ( Screen ):
	skin = """
		<screen name="PictureScreenFull" position="center,center" size="1280,1024" title="Full Screen" backgroundColor="#002C2C39" flags="wfNoBorder">
			<widget name="Full" position="center,center" size="1280,1024" zPosition="1" alphatest="on" />
		</screen>"""
		
	def __init__ (self, session, picPath = None):
		Screen.__init__(self, session)
		print "[PictureScreenfull] __init__\n"
		self.picPath=filename
		self.Scale=AVSwitch().getFramebufferScale()
		self.AutoLoadTimer = eTimer()
		self.AutoLoadTimer.timeout.get().append(self.ShowPicture)
		self.PicLoad=ePicLoad()
		self["Full"]=Pixmap()
		self["myActionMap"]=ActionMap(["WizardActions"],
			{
			"left":self.left,
			"right":self.right,
			"up":self.up,
			"down":self.down,
			"ok":self.cancel,
			"back":self.cancel
			}, -1)

		self.PicLoad.PictureData.get().append(self.DecodePicture)
		self.onLayoutFinish.append(self.ShowPicture)

			
	def ShowPicture(self):
		if self.picPath is not None:
			self.PicLoad.setPara([
						self["Full"].instance.size().width(),
						self["Full"].instance.size().height(),
						self.Scale[0],
						self.Scale[1],0,1,"#002C2C39"])
			self.PicLoad.startDecode(self.picPath)
			self.AutoLoadTimer.start(int(6)*1000)
			
	def DecodePicture(self, PicInfo=""):
		if self.picPath is not None:
			ptr = self.PicLoad.getData()
			self["Full"].instance.setPixmap(ptr)

	def left(self):
		try:
    			urllib.urlopen("%s?command=4" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=5" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def right(self):
		try:
    			urllib.urlopen("%s?command=6" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=7" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def up(self):
		try:
    			urllib.urlopen("%s?command=18" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=19" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def down(self):
		try:
    			urllib.urlopen("%s?command=16" % CONTROL)
			time.sleep(.500)
    			urllib.urlopen("%s?command=17" % CONTROL)
		except:
    			self.session.open(MessageBox, _('Keine Verbindung zur Internet Addresse'), type = MessageBox.TYPE_INFO, timeout = 5 )
		self.ShowPicture()

	def cancel(self):
		print "[PictureScreenFull] - cancel\n"
		
		self.close(ViewFull)
